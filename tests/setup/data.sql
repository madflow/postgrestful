
CREATE EXTENSION IF NOT EXISTS "citext";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

DROP TABLE IF EXISTS datatypes CASCADE;

DROP TYPE IF EXISTS _enum_type;
CREATE TYPE _enum_type AS ENUM ('sad', 'ok', 'happy');

DROP TYPE IF EXISTS _composite_type;
CREATE TYPE _composite_type AS (
    name            text,
    supplier_id     integer,
    price           numeric
);

CREATE TABLE datatypes (
    _smallint SMALLINT,
    _integer INTEGER,
    _bigint BIGINT,
    _decimal DECIMAL,
    _numeric NUMERIC,
    _real REAL,
    _double_precision DOUBLE PRECISION,
    _smallserial SMALLSERIAL,
    _serial SERIAL,
    _bigserial BIGSERIAL,
    _money MONEY,
    _varchar VARCHAR(100),
    _char CHAR(2),
    _text TEXT,
    _bytea BYTEA,
    _timestamp TIMESTAMP WITHOUT TIME ZONE,
    _timestamp_tz TIMESTAMP WITH TIME ZONE,
    _date DATE,
    _time TIME WITHOUT TIME ZONE,
    _time_tz TIME WITH TIME ZONE,
    _interval INTERVAL,
    _boolean BOOLEAN,
    _enum _enum_type,
    _json JSON,
    _jsonb JSONB,
    _textarray TEXT[],
    _integerarray INT[],
    _composite_type _composite_type,
    _int4range int4range,
    _int8range int8range,
    _numrange numrange,
    _tsrange tsrange,
    _tstzrange tstzrange,
    _daterange daterange,
    _uuid UUID,
    _citext citext
);

DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users(
    user_id SERIAL PRIMARY KEY NOT NULL,
    email CITEXT NOT NULL,
    is_admin BOOLEAN NOT NULL
);

DROP TABLE IF EXISTS user_logins;
CREATE TABLE user_logins(
    user_id INTEGER REFERENCES users(user_id) NOT NULL,
    last_login TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

INSERT INTO users (email, is_admin) 
VALUES ('admin@example.com', 't'), ('user@example.com', 'f');

