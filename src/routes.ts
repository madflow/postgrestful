import { JsonSchemaProperties, JsonSchemaProperty } from './interfaces';
import { normalizePath } from './utils';
import debug from './debug';
import { RouteHandler } from 'fastify';
import {
  handlePgClassQuery,
  handlePgClassDocument,
  handlePgClassInsert
} from './handler';
import { PgClass } from 'graphile-build-pg';

export interface Route {
  method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'HEAD' | 'OPTIONS';
  path: string;
  schema: object;
  handler: RouteHandler;
}

const pgTypeIdMap = {
  '20': ['string'],
  '21': ['integer'], // int2
  '23': ['integer'], // int4
  '700': ['number'], // float4
  '701': ['number'], // float8
  '1700': ['number'], // numeric
  '790': ['number'], // money
  '1186': ['number'], // interval
  '114': ['object'], // json
  '3802': ['object'] // jsonb
};

const pgTypeCategoryMap = {
  B: ['boolean']
};

export class PgClassRoute {
  private pgClass: PgClass;
  private routeMap: Array<Route>;

  constructor(pgClass: PgClass) {
    this.pgClass = pgClass;
    this.routeMap = [];
  }

  build() {
    const basePath = `/${normalizePath(this.pgClass.name)}`;

    const properties: JsonSchemaProperties = {};
    const required: Array<string> = [];
    const query = {};
    const bodyValidation: JsonSchemaProperty = {};

    this.pgClass.attributes.forEach(function(att) {
      if (!att.hasDefault && att.isNotNull) {
        required.push(att.name);
      }

      const jsonSchemaType = pgTypeIdMap[att.type.id]
        ? [].concat(pgTypeIdMap[att.type.id])
        : pgTypeCategoryMap[att.type.category]
        ? [].concat(pgTypeCategoryMap[att.type.category])
        : ['string'];

      if (false === att.isNotNull) {
        jsonSchemaType.push('null');
      }

      properties[att.name] = { type: jsonSchemaType };
      properties._links = {
        type: 'object',
        properties: { self: { type: ['string', 'null'] } }
      };
      query[att.name] = { type: jsonSchemaType };
      bodyValidation.type = 'object';
      bodyValidation.required = required;
      bodyValidation.properties = properties;
    });

    const queryResponses = {
      type: 'array',
      items: {
        type: 'object',
        properties
      }
    };

    const responses = {
      200: {
        type: 'object',
        properties
      },
      201: {
        type: 'object',
        properties
      },
      404: {
        type: 'object',
        properties: {
          message: { type: 'string' }
        }
      }
    };

    if (this.pgClass.isSelectable) {
      const queryRoute: Route = {
        method: 'GET',
        path: basePath,
        schema: { responses: queryResponses, query: query },
        handler: async (request, reply) => {
          const result = await handlePgClassQuery(this.pgClass, request);
          reply.headers({ 'Content-Range': '0-1/*' });
          return reply.status(200).send(result.data);
        }
      };

      this.routeMap.push(queryRoute);

      if (this.pgClass.primaryKeyConstraint) {
        const getDocumentRoute: Route = {
          method: 'GET',
          path: `${basePath}/:primaryKey`,
          schema: { response: responses },
          handler: async (request, reply) => {
            const resp = await handlePgClassDocument(this.pgClass, request);
            if (resp) {
              return reply
                .headers(resp.headers)
                .status(resp.status)
                .send(resp.document);
            } else {
              return reply.status(500).send();
            }
          }
        };

        this.routeMap.push(getDocumentRoute);
      }

      if (this.pgClass.isInsertable) {
        const postDocumentRoute: Route = {
          method: 'POST',
          path: basePath,
          schema: { response: responses, query: query, body: bodyValidation },
          handler: async (request, reply) => {
            const { status, document, headers } = await handlePgClassInsert(
              this.pgClass,
              request
            );
            return reply
              .status(status)
              .headers(headers)
              .send(document);
          }
        };
        this.routeMap.push(postDocumentRoute);
      }

      if (this.pgClass.isUpdatable) {
        const putDocumentRoute: Route = {
          method: 'PUT',
          path: basePath,
          schema: { response: responses, query: query },
          handler: async function(request, reply) {
            debug(request);
            reply.send(basePath);
          }
        };
        this.routeMap.push(putDocumentRoute);
      }
    }

    return this.routeMap;
  }
}
