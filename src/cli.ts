import * as yargs from 'yargs';
import { Argv } from 'yargs';
import { AppConfig } from './interfaces';
import { buildApp } from './app';

const serveCommand = async (port: number, schemas: string, connection: any) => {
  console.log(`
  * Listening on port ${port}
  * Introspecting schema(s) ${schemas}
  * Database connection is ${connection}
  `);
  const config: AppConfig = {
    connectionString: connection,
    schemas: schemas.split(',')
  };
  const app = await buildApp(config);
  app.listen(port);
};

yargs
  .command(
    'serve',
    'Start the server.',
    (yargs: Argv) => {
      return yargs
        .option('port', {
          description: 'Port to bind on',
          alias: 'p',
          default: 5000
        })
        .option('connection', {
          description:
            "Database connection string. Defaults to the 'DATABASE_URL' environment variable",
          alias: 'c',
          default: process.env.DATABASE_URL
        })
        .option('schemas', {
          description: 'Comma seperated list of schema names to introspect',
          alias: 's',
          default: 'public'
        });
    },
    argv => {
      serveCommand(argv.port, argv.schemas, argv.connection);
    }
  )

  .string('connection').argv;
