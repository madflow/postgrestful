import { pool } from './db';
import * as sql from 'pg-sql2';
import { PgClass } from 'graphile-build-pg';
import { FastifyRequest } from 'fastify';
import { normalizePath } from './utils';

interface QueryResponse {
  data: Array<any>;
  totalCount: Number;
  limit: Number,
  offset: Number
}

function orderPart(tableName: string, order: string) {
  if (!order) {
    return sql.query``;
  }
  let parts = order.split(',');
  let sub = parts.map(function(part) {
    let fieldDirection = part.split('.');
    return sql.query`${sql.identifier(
      tableName
    )}.${sql.identifier(fieldDirection[0])} ${sql.raw(fieldDirection[1])}`;
  });
  return sql.query` ORDER BY ${sql.join(sub, ', ')}`;
}

export const handlePgClassQuery = async (
  pgClass: PgClass,
  request: FastifyRequest
) => {
  const schemaName = pgClass.namespaceName;
  const tableName = pgClass.name;
  const fields = pgClass.attributes.map(attr => attr.name);

  const sqlFields = sql.join(
    fields.map(fieldName => sql.identifier(tableName, fieldName)),
    ', '
  );

  let limit;
  let offset;
  let limitQuery = sql.query``;
  let offsetQuery = sql.query``;
  let orderQuery = sql.query``;
  const query:any = request.query;
  const queryConditions = [];

  for (const condition of Object.entries(query)) {
    const ident: any = condition[0];
    const value: any = condition[1];

    if (ident.toLowerCase() === 'limit') {
      limitQuery = sql.query` LIMIT ${sql.value(value)}`;
      limit = value;
      continue;
    }

    if (ident.toLowerCase() === 'offset') {
      offsetQuery = sql.query` OFFSET ${sql.value(value)}`;
      offset = value;
      continue;
    }

    if (ident.toLowerCase() === 'order') {
      orderQuery = orderPart(tableName, value);
      continue;
    }

    queryConditions.push(
      sql.query`${sql.identifier(ident)}=${sql.value(value)}`
    );
  }

  const wheres = sql.join(queryConditions, 'AND');

  const sqlConditions =
    wheres.length > 0 ? sql.query` WHERE ${wheres}` : sql.query``;

  const innerQuery = sql.query`SELECT ${sqlFields} FROM ${sql.identifier(
    schemaName
  )}.${sql.identifier(tableName)}${sqlConditions}${orderQuery}${limitQuery}${offsetQuery}`;

  const aggregatesQuery = sql.query`SELECT count(1) AS "totalCount" FROM ${sql.identifier(
    schemaName
  )}.${sql.identifier(tableName)} ${sqlConditions}`;

  const sqlAlias = sql.identifier(Symbol());
  const aggregatesAlias = sql.identifier(Symbol());

  const parts = sql.query`
    WITH ${sqlAlias} AS (${innerQuery}),
    ${aggregatesAlias} AS (${aggregatesQuery})
    SELECT json_agg(to_json(${sqlAlias}.*)) AS "data", 
    to_json((SELECT "totalCount" FROM ${aggregatesAlias})) AS "totalCount"
    FROM ${sqlAlias}
`;

  const { text, values } = sql.compile(parts);

  const { rows } = await pool.query(text, values);

  const resp: QueryResponse = {
    data: rows[0].data,
    totalCount: rows[0].totalCount,
    limit,
    offset
  };

  return resp;
};

export const handlePgClassDocument = async (
  pgClass: PgClass,
  request: FastifyRequest
) => {
  const schemaName = pgClass.namespaceName;
  const tableName = pgClass.name;
  const fields = pgClass.attributes.map(attr => attr.name);
  const params: any = request.params;

  if (pgClass.primaryKeyConstraint) {
    const primaryKey = pgClass.primaryKeyConstraint.keyAttributes[0].name;
    const ident = sql.identifier(primaryKey);
    const value = sql.value(params.primaryKey);
    const where = sql.query`${ident}=${value}`;

    const sqlFields = sql.join(
      fields.map(fieldName => sql.identifier(tableName, fieldName)),
      ', '
    );

    const innerQuery = sql.query`SELECT ${sqlFields} FROM ${sql.identifier(
      schemaName
    )}.${sql.identifier(tableName)} WHERE ${where}`;

    const sqlAlias = sql.identifier(Symbol());

    const parts = sql.query`
          WITH ${sqlAlias} AS (${innerQuery}) SELECT * FROM ${sqlAlias}
      `;

    const { text, values } = sql.compile(parts);

    const { rows } = await pool.query(text, values);

    const document = rows[0] || null;

    if (null === document) {
      const notFound = {
        headers: {},
        status: 404,
        document: { message: 'The resource cannot be found at this location' }
      };
      return notFound;
    }

    const headers = {};
    const links = { _links: {} };

    if (pgClass.primaryKeyConstraint) {
      const primaryKey = pgClass.primaryKeyConstraint.keyAttributes[0].name;
      const self = `/${normalizePath(pgClass.name)}/${document[primaryKey]}`;
      links._links['self'] = self;
      headers['content-location'] = self;
    }

    const blueprint = {
      _links: {}
    };

    const response = {
      status: 200,
      document: {
        ...blueprint,
        ...links,
        ...document
      },
      headers: headers
    };

    return response;
  }
};

export const handlePgClassInsert = async (
  pgClass: PgClass,
  request: FastifyRequest
) => {
  const schemaName = pgClass.namespaceName;
  const tableName = pgClass.name;
  const fields = pgClass.attributes.map(attr => attr.name);
  const insertMap = {};
  const body: any = request.body;
  fields.forEach(field => {
    if (body[field] !== undefined) {
      insertMap[field] = body[field];
    }
  });

  const sqlFields = sql.join(
    Object.keys(insertMap).map((fieldName: string) =>
      sql.identifier(fieldName)
    ),
    ', '
  );

  const sqlValues = sql.join(
    Object.entries(insertMap).map((mapEntry: any) => sql.value(mapEntry[1])),
    ', '
  );

  const innerQuery = sql.query`INSERT INTO ${sql.identifier(
    schemaName
  )}.${sql.identifier(
    tableName
  )} (${sqlFields}) VALUES (${sqlValues}) RETURNING *
    `;

  const sqlAlias = sql.identifier(Symbol());

  const parts = sql.query`
        WITH ${sqlAlias} AS (${innerQuery}) SELECT * FROM ${sqlAlias}
    `;

  const { text, values } = sql.compile(parts);

  const { rows } = await pool.query(text, values);
  const document = rows[0] || {};

  const links = { _links: {} };
  const headers = {};

  if (pgClass.primaryKeyConstraint) {
    const primaryKey = pgClass.primaryKeyConstraint.keyAttributes[0].name;
    const self = `/${normalizePath(pgClass.name)}/${document[primaryKey]}`;
    links._links['self'] = self;
    headers['content-location'] = self;
  }

  const blueprint = {
    _links: {}
  };

  const response = {
    status: 201,
    document: { ...blueprint, ...links, ...document },
    headers: headers
  };

  return response;
};
