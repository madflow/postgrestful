import pluralize from 'pluralize';

export const normalizePath = (str: string) => {
  return pluralize(str.replace(/[^a-z0-9]/gi, '').toLowerCase());
};

