import { SchemaBuilder } from 'graphile-build';
import { PgNamespace, PgClass } from 'graphile-build-pg';
import {
  OpenApiPathItem,
  OpenApiOperation,
  OpenApiResponse,
  OpenApiResponses,
  OpenApiParameter,
  OpenApiReference,
  OpenApi,
  OpenApiPaths,
  OpenApiComponents
} from './interfaces';

interface Api extends OpenApi {}
interface ApiComponents extends OpenApiComponents {}
interface ApiPaths extends OpenApiPaths {}
interface ApiPathItem extends OpenApiPathItem {}
interface ApiOperation extends OpenApiOperation {}
interface ApiResponse extends OpenApiResponse {}
interface ApiResponses extends OpenApiResponses {}
interface ApiParameter extends OpenApiParameter {}

export async function build(schema: SchemaBuilder) {
  const build = schema.createBuild();
  const pgIntrospectionResultsByKind = build.pgIntrospectionResultsByKind;
  const namespaceNames = pgIntrospectionResultsByKind.namespace.map(
    (n: PgNamespace) => n.name
  );
  const tables = pgIntrospectionResultsByKind.class.filter((c: PgClass) => {
    return namespaceNames.includes(c.namespaceName) && c.classKind === 'r';
  });

  const defaultQueryParameters: Record<string, ApiParameter> = {
    order: {
      name: 'order',
      in: 'query',
      required: false,
      description: 'Order the result by one ore more order expressions',
      schema: {
        type: 'string'
      }
    },
    limit: {
      name: 'limit',
      in: 'query',
      required: false,
      description: 'Limits the result',
      schema: {
        type: 'number'
      }
    },
    offset: {
      name: 'offset',
      in: 'query',
      required: false,
      description: 'Offset of the result',
      schema: {
        type: 'number'
      }
    }
  };

  const paths: ApiPaths = tables.reduce((paths: ApiPaths, c: PgClass) => {
    const ok: ApiResponse = {
      description: 'OK'
    };

    const responses: ApiResponses = {
      '200': ok
    };

    const parameters = Object.entries(defaultQueryParameters).map(([key]) => {
      const ref: OpenApiReference = {
        $ref: `#/components/parameters/${key}`
      };
      return ref;
    });

    const get: ApiOperation = {
      description: c.comment || undefined,
      tags: [c.name],
      responses: responses,
      parameters: parameters
    };

    const pathItem: ApiPathItem = {
      get: get
    };
    return { ...paths, [`/${c.name}`]: pathItem}
  }, {});

  const parameters = { ...defaultQueryParameters };
  const components: ApiComponents = {
    parameters: parameters
  };
  const api: Api = {
    openapi: '3.0.2',
    info: {
      title: 'Postgrestful API',
      version: '0.1'
    },
    paths: paths,
    components: components
  };

  return api;
}
