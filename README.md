# Postgrestful

> An instant RESTful Api from any Postgresql Database.

## Installation

### Global

```BASH
yarn global add postgrestful
```

```BASH
npm install postgrestful -g
```

### Using Docker

```BASH
docker pull postgrestful/postgrestful
docker run --init postgrestful/postgrestful --help
```

### As a library

```BASH
yarn global postgrestful
```

```BASH
npm install postgrestful
```

```JS
const {postgrestful} = require('postgrestful');
```

## Testing

```BASH
export DATABASE_URL=postgresql://postgres:postgres@localhost:5432/postgres
```

```BASH
yarn run test:setup
yarn run test:seed
yarn run test
```

## API Documentation

The Api is automatically documented using the [OpenAPI](https://openapis.org/) standard.

The output format is `Json`. You can import the Open Api defininition in any tool that supports `v3` of the format.
The documentation is served under the route `/_api`.

### Open Api Tools

- https://editor.swagger.io/
