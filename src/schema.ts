import { defaultPlugins, getBuilder } from 'graphile-build';
import { defaultPlugins as pgDefaultPlugins } from 'graphile-build-pg';

export async function getSchema(
  pgConfig = process.env.DATABASE_URL,
  pgSchemas = ['public'],
  additionalPlugins = []
) {
  return getBuilder(
    [...defaultPlugins, ...pgDefaultPlugins, ...additionalPlugins],
    {
      pgConfig,
      pgSchemas,
      pgExtendedTypes: true
    }
  );
}
