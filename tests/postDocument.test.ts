import path from 'path';
import fs from 'fs';
import supertest from 'supertest';
import { buildApp } from '../src/app';
import { AppConfig } from '../src/interfaces';
import { FastifyInstance } from 'fastify';
import { pool } from '../src/db';

const config: AppConfig = {
  connectionString:
    process.env.DATABASE_URL ||
    'postgresql://postgres:postgres@localhost:5432/postgres',
  schemas: ['public']
};

let app: FastifyInstance;

afterAll(async () => {
  const dataSql = fs
    .readFileSync(path.dirname(__filename) + '/setup/data.sql')
    .toString();
  await pool.query(dataSql);
  await app.close();
  await pool.end();
});

beforeAll(async () => {
  app = await buildApp(config);
  await app.ready();
});

describe('POST /endpoint', () => {
  it('should respond with a 404 when route does not exist', async () => {
    await supertest(app.server)
      .post('/guuuuuu')
      .expect(404)
      .expect('Content-Type', 'application/json; charset=utf-8');
  });
  it('should respond with a 400 when route does but there is an validation error', async () => {
    await supertest(app.server)
      .post('/users')
      .expect(400)
      .expect('Content-Type', 'application/json; charset=utf-8');
  });
  it('should respond with a 201 when there is correct data', async () => {
    await supertest(app.server)
      .post('/users')
      .send({ email: 'tester12345@example.com', is_admin: false })
      .expect(201)
      .expect('Content-Type', 'application/json; charset=utf-8');
  });
  it('should respond with a document payload', async () => {
    const res = await supertest(app.server)
      .post('/users')
      .send({ email: 'tester5432@example.com', is_admin: false });
    expect(res.body.email).toBe('tester5432@example.com');
    expect(res.body.is_admin).toBe(false);
  });
  it('should respond with a self link that redirects to the document route', async () => {
    const res = await supertest(app.server)
      .post('/users')
      .send({ email: 'tester8543@example.com', is_admin: false });
    const selfLink = res.body._links.self;
    const doc = await supertest(app.server).get(selfLink);
    expect(doc.body.email).toBe('tester8543@example.com');
    expect(doc.body.is_admin).toBe(false);
  });
  it('should respond with content location header redirects to the document route', async () => {
    const res = await supertest(app.server)
      .post('/users')
      .send({ email: 'tester1000@example.com', is_admin: true });
    const contentLocation = res.header['content-location'];
    const doc = await supertest(app.server).get(contentLocation);
    expect(doc.body.email).toBe('tester1000@example.com');
    expect(doc.body.is_admin).toBe(true);
  });
});
