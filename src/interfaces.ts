import {JSONSchema7Type} from 'json-schema';

export interface AppConfig {
  connectionString: string;
  schemas: Array<string>;
}
export interface JsonSchemaProperty {
  [key: string]: any;
}

export interface JsonSchemaProperties {
  [key: string]: JsonSchemaProperty;
};

export interface OpenApi {
  openapi: string;
  info: OpenApiInfo;
  servers?: Array<OpenApiServer>;
  paths: OpenApiPaths;
  components?: OpenApiComponents;
  security?: OpenApiSecurityRequirement;
  tags?: OpenApiTag;
}

export interface OpenApiComponents {
  schemas?: Record<string, OpenApiSchema>;
  responses?: Record<string, OpenApiResponse>;
  parameters?: Record<string, OpenApiParameter>;
  examples?: Record<string, OpenApiExample>;
}

export interface OpenApiTag {
  name: string;
  description?: string;
  externalDocs?: OpenApiExternalDocumentation;
}

export interface OpenApiInfo {
  title: string;
  description?: string;
  termsOfService?: string;
  contact?: OpenApiContact;
  license?: OpenApiLicense;
  version: string;
}

export interface OpenApiContact {
  name?: string;
  url?: string;
  email?: string;
}

export interface OpenApiLicense {
  name: string;
  url?: string;
}

export interface OpenApiServer {
  url?: string;
  description?: string;
}

export interface OpenApiPaths {
  [key: string]: OpenApiPathItem;
}

export interface OpenApiPathItem {
  $ref?: string;
  summary?: string;
  description?: string;
  get?: OpenApiOperation;
  put?: OpenApiOperation;
  post?: OpenApiOperation;
  delete?: OpenApiOperation;
  options?: OpenApiOperation;
  head?: OpenApiOperation;
  patch?: OpenApiOperation;
  trace?: OpenApiOperation;
  servers?: Array<OpenApiServer>;
  parameters?: OpenApiParameter;
}

export interface OpenApiOperation {
  tags?: Array<string>;
  summary?: string;
  description?: string;
  externalDocs?: OpenApiExternalDocumentation;
  operationId?: string;
  parameters?: Array<OpenApiParameter | OpenApiReference>;
  requestBody?: OpenApiRequestBody | OpenApiReference;
  responses: OpenApiResponses;
  callbacks?: OpenApiCallbackMap;
  deprecated?: boolean;
  security?: OpenApiSecurityRequirement;
  servers?: OpenApiServer;
}

export interface OpenApiSecurityRequirement {}

export interface OpenApiExternalDocumentation {
  url?: string;
  description?: string;
}

export interface OpenApiParameter {
  name: string;
  in: 'query' | 'header' | 'path' | 'cookie';
  description?: string;
  required: boolean;
  deprecated?: boolean;
  allowEmptyValue?: boolean;
  style?: string;
  explode?: boolean;
  allowReserved?: boolean;
  schema?: OpenApiSchema | OpenApiReference;
  example?: any;
  examples?: Array<OpenApiExample | OpenApiReference>;
  content?: OpenApiMediaTypeMap;
}

export interface OpenApiReference {
  $ref: string;
}

export interface OpenApiMediaTypeMap {
  [key: string]: OpenApiMediaType;
}

export interface OpenApiMediaType {
  schema?: OpenApiSchema | OpenApiReference;
  example?: any;
  examples: Array<OpenApiExample | OpenApiReference>;
  encoding: OpenApiEncodingMap;
}

export interface OpenApiExample {
  summary?: string;
  description?: string;
  value?: any;
  externalValue?: string;
}

export interface OpenApiSchema {
  type: JSONSchema7Type|Array<JSONSchema7Type>
  required?: boolean;
  default?: any;
  properties?: OpenApiSchema;
}

export interface OpenApiRequestBody {
  description?: string;
  content: OpenApiMediaTypeMap;
  required?: boolean;
  responses: OpenApiResponses;
}

export interface OpenApiResponses {
  [httpStatusCode: string]: OpenApiResponse|OpenApiReference;
}

export interface OpenApiCallbackMap {
  [key: string]: string;
}

export interface OpenApiResponse {
  description: string;
  headers?: OpenApiHeaderMap | OpenApiReference;
  content?: OpenApiMediaTypeMap | OpenApiReference;
  links?: OpenApiLinkMap;
}

export interface OpenApiLinkMap {
  [name: string]: OpenApiLink;
}

export interface OpenApiLink {
  operationRef?: string;
  operationId?: string;
  parameters?: OpenApiExpressionMap;
  requestBody?: any;
  description?: string;
  server?: OpenApiServer;
}

export interface OpenApiExpressionMap {
  [key: string]: string;
}

export interface OpenApiHeaderMap {
  [header: string]: OpenApiHeader;
}

export interface OpenApiHeader {
  description?: string;
  required: boolean;
  deprecated?: boolean;
  allowEmptyValue?: boolean;
  style?: string;
  explode?: boolean;
  allowReserved?: boolean;
  schema?: OpenApiSchema | OpenApiReference;
  example?: any;
  examples?: Array<OpenApiExample | OpenApiReference>;
  content?: OpenApiMediaTypeMap;
}

export interface OpenApiEncodingMap {
  [schemaProperty: string]: OpenApiEncoding;
}

export interface OpenApiEncoding {
  contentType?: string;
  headers?: OpenApiHeaderMap;
  style?: string;
  explode?: boolean;
  allowReserved?: boolean;
}


