import { Client } from 'pg';
import { getSchema } from './schema';
import { PgClassRoute, Route } from './routes';

async function generateRestRoutes(
  connectionString: string | undefined,
  schemas: string[]
) {
  const client = new Client(connectionString);
  const schema = await getSchema(connectionString, schemas);
  const build = schema.createBuild();
  const introspectionResult = build.pgIntrospectionResultsByKind;
  const routes: Array<Route> = [];
  for (const klass of introspectionResult.class) {
    if (schemas.includes(klass.namespaceName)) {
      const route = new PgClassRoute(klass);
      const assembled = route.build();
      assembled.forEach(function(route: Route) {
        routes.push(route);
      });
    }
  }
  client.end();
  return routes;
}

export { generateRestRoutes };
