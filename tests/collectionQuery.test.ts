import supertest from 'supertest';
import { buildApp } from '../src/app';
import { AppConfig } from '../src/interfaces';
import { FastifyInstance } from 'fastify';
import { pool } from '../src/db';

const config: AppConfig = {
  connectionString:
    process.env.DATABASE_URL ||
    'postgresql://postgres:postgres@localhost:5432/postgres',
  schemas: ['public']
};

let app: FastifyInstance;

afterAll(async () => {
  await app.close();
  await pool.end();
});

beforeAll(async () => {
  app = await buildApp(config);
  await app.ready();
});

describe('GET /endpoint', () => {
  it('should return correct status code and content-type', async () => {
    await supertest(app.server)
      .get('/users')
      .expect(200)
      .expect('Content-Type', 'application/json; charset=utf-8');
  });
  it('should return correct number of documents and content', async () => {
    const res = await supertest(app.server).get('/users?order=email.asc');
    expect(res.body).toBeInstanceOf(Array);
    expect(res.body.length).toBe(2);
    expect(res.body[0].email).toBe('admin@example.com');
    expect(res.body[0].is_admin).toBe(true);
  });
  it('should return correct document when ordering', async () => {
    const res = await supertest(app.server).get('/users?order=email.asc');
    expect(res.body[0].email).toBe('admin@example.com');
    expect(res.body[0].is_admin).toBe(true);
    const res2 = await supertest(app.server).get('/users?order=email.desc');
    expect(res2.body[0].email).toBe('user@example.com');
    expect(res2.body[0].is_admin).toBe(false);
    const res3 = await supertest(app.server).get('/users?order=user_id.desc,email.asc');
    expect(res3.body[0].email).toBe('user@example.com');
    expect(res3.body[0].is_admin).toBe(false);
  });
  it('should allow case insensitive order', async () => {
    const res = await supertest(app.server).get('/users?ordeR=email.asC');
    expect(res.body[0].email).toBe('admin@example.com');
    const res2 = await supertest(app.server).get('/users?ordeR=email.Desc');
    expect(res2.body[0].email).toBe('user@example.com');
  });
  it('should allow simple filtering by property', async () => {
    const res = await supertest(app.server).get('/users?is_admin=true&email=admin@example.com');
    expect(res.body).toBeInstanceOf(Array);
    expect(res.body.length).toBe(1);
  });
  it('should allow limit and offset', async () => {
    const res = await supertest(app.server).get('/users?limit=1&offset=1');
    expect(res.body).toBeInstanceOf(Array);
    expect(res.body.length).toBe(1);
    const res2 = await supertest(app.server).get('/users?limit=10&offset=0');
    expect(res2.body).toBeInstanceOf(Array);
    expect(res2.body.length).toBe(2);
  });
  it('should allow limit and offset case insensitive', async () => {
    const res = await supertest(app.server).get('/users?limiT=1&offsEt=1');
    expect(res.body).toBeInstanceOf(Array);
    expect(res.body.length).toBe(1);
  });
  it('should allow a combination of limit, offset, order and property filters', async () => {
    const res = await supertest(app.server).get('/users?limit=1&offset=0&order=user_id.desc,email.asc&email=admin@example.com');
    expect(res.body).toBeInstanceOf(Array);
    expect(res.body.length).toBe(1);
    expect(res.body[0].email).toBe('admin@example.com');
  });
});
