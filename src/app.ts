import fastify, { FastifyInstance, FastifyPluginOptions } from 'fastify';
import fastifyPlugin from 'fastify-plugin';
import debug from './debug';
import { build as buildOpenApi } from './api';

import { generateRestRoutes } from './generator';
import { AppConfig } from './interfaces';
import { getSchema } from './schema';

export async function buildApp(config: AppConfig) {
  const routes = await generateRestRoutes(
    config.connectionString,
    config.schemas
  );

  const app = fastify();

  const plugin = fastifyPlugin(async (server: FastifyInstance, opts: FastifyPluginOptions, next: Function) => {
    for (const route of routes) {
      server.route({
        url: route.path,
        method: route.method,
        handler: route.handler,
        schema: route.schema
      });
    }
    debug(opts);
    next();
  });

  app.register(plugin);

  app.get('/', async function() {
    return {
      status: 'OK'
    };
  });

  app.get('/_api', async function() {
    const schema = await getSchema(config.connectionString, config.schemas);
    const api = await buildOpenApi(schema);
    return api;
  });

  return app;
}
