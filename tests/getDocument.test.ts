import supertest from 'supertest';
import { buildApp } from '../src/app';
import { AppConfig } from '../src/interfaces';
import { FastifyInstance } from 'fastify';
import { pool } from '../src/db';

const config: AppConfig = {
  connectionString:
    process.env.DATABASE_URL ||
    'postgresql://postgres:postgres@localhost:5432/postgres',
  schemas: ['public']
};

let app: FastifyInstance;

afterAll(async () => {
  await app.close();
  await pool.end();
});

beforeAll(async () => {
  app = await buildApp(config);
  await app.ready();
});

describe('GET /endpoint/:primaryKey', () => {
  it('should respond with a 404 when not present', async () => {
    await supertest(app.server)
      .get('/datatypes/1')
      .expect(404)
      .expect('Content-Type', 'application/json; charset=utf-8');
  });
  it('should respond with a 200 when present', async () => {
    await supertest(app.server)
      .get('/users/1')
      .expect(200)
      .expect('Content-Type', 'application/json; charset=utf-8');
  });
  it('should respond with a correct document', async () => {
    const res = await supertest(app.server).get('/users/1');
    expect(res.body).toBeInstanceOf(Object);
    expect(res.body.email).toBe('admin@example.com');
    expect(res.body.is_admin).toBe(true);
  });
  it('should contain a self link', async () => {
    const res = await supertest(app.server).get('/users/1');
    expect(res.body._links.self).toBe('/users/1');
  });
  it('should yield a 404 when primary key is not present', async () => {
    await supertest(app.server)
      .get('/users/999')
      .expect(404)
      .expect('Content-Type', 'application/json; charset=utf-8');
  });
  it('should contain a content location self link', async () => {
    await supertest(app.server)
      .get('/users/1')
      .expect('Content-Location', '/users/1');
  });
});
