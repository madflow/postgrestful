import supertest from 'supertest';
import { buildApp } from '../src/app';
import { AppConfig } from '../src/interfaces';
import { FastifyInstance } from 'fastify';
import { pool } from '../src/db';

const config: AppConfig = {
  connectionString: process.env.DATABASE_URL || 'postgresql://postgres:postgres@localhost:5432/postgres',
  schemas: ['public']
};

let app: FastifyInstance;

afterAll(async () => {
  await app.close();
  await pool.end();
});

beforeAll(async () => {
  app = await buildApp(config);
  await app.ready();
});

describe('GET /', () => {
  it('should ', async () => {
    await supertest(app.server)
      .get('/')
      .expect(200)
      .expect('Content-Type', 'application/json; charset=utf-8');
  });
});
